---
title: Windows系统使用自定义快捷键进入休眠状态的简单方法 # `Markdown`的文件标题文章标题，强烈建议填写此选项
date: 2023-02-07 # 文件创建时的日期时间发布时间，强烈建议填写此选项，且最好保证全局唯一
# updated: # 更新时间
comments: true
tags: 
- Windows技巧
- 休眠
categories:
- Windows技巧
layout: tweet
icon: balloon
---
# Windows系统使用自定义快捷键进入休眠状态的简单方法
## 为什么要休眠
电脑功率300W，虽说不算高但生活还是抠唆一点好，Windows系统睡眠状态虽然方便快速，但内存依然耗电，并且如果遇到断电，数据会丢失。而休眠模式将数据保存到硬盘，然后所有硬件处于断电状态，更省电且不丢数据，开机后恢复原先运行状态，岂不美哉！ 想要的是按快捷键进入休眠状态，这样最便捷。然而并没有搜索到相关教程，自己摸索了下，达到了预期效果。
## 过程
### 1.使用cmd命令进入休眠（可不看）
先想着用命令休眠，按Windows徽标键+r运行cmd进入命令提示符，输入：
```cmd
shutdown -h
```
按回车即可 

<img src="https://pic3.zhimg.com/80/v2-35c01346cd3c2978329e32f99e695712_720w.webp"/>

### 2.桌面创建bat批处理文件实现休眠
发现把命令写进bat文件可以更快，在桌面新建一个shutdown（可自定名称）.txt文件，写入内容：
```cmd
e:
shutdown -h
```
后保存，改名为shutdown.bat，双击此文件即可休眠

<img src="https://picx.zhimg.com/80/v2-30fb33da47e1b08d2ae1f7442ba13da9_720w.png?source=d16d100b"/>
<img src="https://picx.zhimg.com/80/v2-f8eade3172a77e7f4bcdb2c4656f5c48_720w.png?source=d16d100b"/>

### 3.使用快捷键实现休眠
Windows可以按自定义快捷键打开应用（快捷方式），把上一步创建好的.bat文件放到一个桌面和回收站以外的其它地方，右键该文件，显示更多选项，发送到桌面快捷方式 
​<img src="https://pic1.zhimg.com/80/v2-012772bf4f64db6f16ccf8c813cd3ef6_720w.png?source=d16d100b"/>

 改名为休眠（可自定名称） 
​

<img src="https://pic1.zhimg.com/80/v2-5d0d9746f6c2726a569a6ce2a7e2ac78_720w.png?source=d16d100b"/>

 右键该快捷方式，点属性，在快捷键那里按Crtl+Alt+X(可自定，X意“休”比较好记)并应用 

<img src="https://picx.zhimg.com/80/v2-aef1a446f6a8f22c2ec2a43a4c335bf0_720w.png?source=d16d100b"/>

 现在按Crtl+Alt+X即可休眠